'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">stam2 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-50f0ff1a622a75970e37a27c990968392ce9c8596f594e55de1416191c890b432a05919756831b8f4aedab0a3512a84b71366993ad21e8b5c6d2df7475d5bd2c"' : 'data-target="#xs-components-links-module-AppModule-50f0ff1a622a75970e37a27c990968392ce9c8596f594e55de1416191c890b432a05919756831b8f4aedab0a3512a84b71366993ad21e8b5c6d2df7475d5bd2c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-50f0ff1a622a75970e37a27c990968392ce9c8596f594e55de1416191c890b432a05919756831b8f4aedab0a3512a84b71366993ad21e8b5c6d2df7475d5bd2c"' :
                                            'id="xs-components-links-module-AppModule-50f0ff1a622a75970e37a27c990968392ce9c8596f594e55de1416191c890b432a05919756831b8f4aedab0a3512a84b71366993ad21e8b5c6d2df7475d5bd2c"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogModule.html" data-type="entity-link" >CatalogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CatalogModule-fb409d2668c72a52bae5e1618cf24db91aaac244dc9bfaa0c530a737cd2b4cd79ecd0d4ac530e8a10d9e61ee56effa67253bf8b238c25ab16e9f5659dc145616"' : 'data-target="#xs-components-links-module-CatalogModule-fb409d2668c72a52bae5e1618cf24db91aaac244dc9bfaa0c530a737cd2b4cd79ecd0d4ac530e8a10d9e61ee56effa67253bf8b238c25ab16e9f5659dc145616"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CatalogModule-fb409d2668c72a52bae5e1618cf24db91aaac244dc9bfaa0c530a737cd2b4cd79ecd0d4ac530e8a10d9e61ee56effa67253bf8b238c25ab16e9f5659dc145616"' :
                                            'id="xs-components-links-module-CatalogModule-fb409d2668c72a52bae5e1618cf24db91aaac244dc9bfaa0c530a737cd2b4cd79ecd0d4ac530e8a10d9e61ee56effa67253bf8b238c25ab16e9f5659dc145616"' }>
                                            <li class="link">
                                                <a href="components/CatalogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogRoutingModule.html" data-type="entity-link" >CatalogRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-367715f9f015a0b106aee2ca56a42ceee283a11849022e528aeb1331b9193f8e565dc1114284c20891da411357e3418631e443034fc039b27abe97ef22bdb8b8"' : 'data-target="#xs-components-links-module-CoreModule-367715f9f015a0b106aee2ca56a42ceee283a11849022e528aeb1331b9193f8e565dc1114284c20891da411357e3418631e443034fc039b27abe97ef22bdb8b8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-367715f9f015a0b106aee2ca56a42ceee283a11849022e528aeb1331b9193f8e565dc1114284c20891da411357e3418631e443034fc039b27abe97ef22bdb8b8"' :
                                            'id="xs-components-links-module-CoreModule-367715f9f015a0b106aee2ca56a42ceee283a11849022e528aeb1331b9193f8e565dc1114284c20891da411357e3418631e443034fc039b27abe97ef22bdb8b8"' }>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-b562ce39762cfbab65d4963dc2f2dc9a48ddf0957a6fb73dd4c35f3559caa05e6fac4a719333a38e3984d083db17383670d667b08992a29692737a02e10a6168"' : 'data-target="#xs-components-links-module-LoginModule-b562ce39762cfbab65d4963dc2f2dc9a48ddf0957a6fb73dd4c35f3559caa05e6fac4a719333a38e3984d083db17383670d667b08992a29692737a02e10a6168"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-b562ce39762cfbab65d4963dc2f2dc9a48ddf0957a6fb73dd4c35f3559caa05e6fac4a719333a38e3984d083db17383670d667b08992a29692737a02e10a6168"' :
                                            'id="xs-components-links-module-LoginModule-b562ce39762cfbab65d4963dc2f2dc9a48ddf0957a6fb73dd4c35f3559caa05e6fac4a719333a38e3984d083db17383670d667b08992a29692737a02e10a6168"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link" >LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' : 'data-target="#xs-components-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' :
                                            'id="xs-components-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChartjsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChartjsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ColComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ColComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HelloComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HelloComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LeafletComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LeafletComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RowComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RowComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' : 'data-target="#xs-directives-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' :
                                        'id="xs-directives-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                        <li class="link">
                                            <a href="directives/BorderDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BorderDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfLoggedDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfLoggedDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfRoleIsDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfRoleIsDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfSigninDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfSigninDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/LoaderDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoaderDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MarginDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MarginDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MyRouterLinkDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MyRouterLinkDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/PadDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PadDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/StopPropagationDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StopPropagationDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/UrlDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UrlDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' : 'data-target="#xs-pipes-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' :
                                            'id="xs-pipes-links-module-SharedModule-1732e5666660b8d77ae852c36b0826b11656ff8f9eb0ea7608bbf82fd5de1a1fae05b190b2be12c65cbf5aa132287953898c00ebf64185490e198c935c6f341f"' }>
                                            <li class="link">
                                                <a href="pipes/FormatPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormatPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TimesAgoPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TimesAgoPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit1Module.html" data-type="entity-link" >Uikit1Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Uikit1Module-a4f8e700c430f322862bcdd9880181bc8c6f32935d2c2f08bd8ce184d775f63b43910afc5af1cdfe3957d756d0931940569548b701e81dba4418ecbb1248958d"' : 'data-target="#xs-components-links-module-Uikit1Module-a4f8e700c430f322862bcdd9880181bc8c6f32935d2c2f08bd8ce184d775f63b43910afc5af1cdfe3957d756d0931940569548b701e81dba4418ecbb1248958d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Uikit1Module-a4f8e700c430f322862bcdd9880181bc8c6f32935d2c2f08bd8ce184d775f63b43910afc5af1cdfe3957d756d0931940569548b701e81dba4418ecbb1248958d"' :
                                            'id="xs-components-links-module-Uikit1Module-a4f8e700c430f322862bcdd9880181bc8c6f32935d2c2f08bd8ce184d775f63b43910afc5af1cdfe3957d756d0931940569548b701e81dba4418ecbb1248958d"' }>
                                            <li class="link">
                                                <a href="components/Uikit1Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Uikit1Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit1RoutingModule.html" data-type="entity-link" >Uikit1RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit2Module.html" data-type="entity-link" >Uikit2Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Uikit2Module-566f0b4229255837253ae9d8afb5826f07c149c016f64b739e5b904d3abf40a4dca3aad0bab154b6f5bfbf571e2e153aa4cd2857bc4674976e558d81a364eeb8"' : 'data-target="#xs-components-links-module-Uikit2Module-566f0b4229255837253ae9d8afb5826f07c149c016f64b739e5b904d3abf40a4dca3aad0bab154b6f5bfbf571e2e153aa4cd2857bc4674976e558d81a364eeb8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Uikit2Module-566f0b4229255837253ae9d8afb5826f07c149c016f64b739e5b904d3abf40a4dca3aad0bab154b6f5bfbf571e2e153aa4cd2857bc4674976e558d81a364eeb8"' :
                                            'id="xs-components-links-module-Uikit2Module-566f0b4229255837253ae9d8afb5826f07c149c016f64b739e5b904d3abf40a4dca3aad0bab154b6f5bfbf571e2e153aa4cd2857bc4674976e558d81a364eeb8"' }>
                                            <li class="link">
                                                <a href="components/Uikit2Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Uikit2Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit2RoutingModule.html" data-type="entity-link" >Uikit2RoutingModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CatalogService.html" data-type="entity-link" >CatalogService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Address.html" data-type="entity-link" >Address</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Auth.html" data-type="entity-link" >Auth</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Company.html" data-type="entity-link" >Company</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Config.html" data-type="entity-link" >Config</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Geo.html" data-type="entity-link" >Geo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User-1.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});