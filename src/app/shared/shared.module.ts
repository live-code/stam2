import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {COMPONENTS} from "./components";
import {PadDirective} from "./directives/pad.directive";
import { MarginDirective } from './directives/margin.directive';
import { BorderDirective } from './directives/border.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkDirective } from './directives/my-router-link.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import {IfRoleIsDirective} from "./directives/if-role-is.directive";
import { LoaderDirective } from './directives/loader.directive';
import { FormatPipe } from './pipes/format.pipe';
import { TimesAgoPipe } from './pipes/times-ago.pipe';
import {CardComponent} from "./components/card.component";
import {GroupComponent} from "./components/group.component";
import {AccordionComponent} from "./components/accordion.component";
import {RowComponent} from "./components/row.component";
import {ColComponent} from "./components/col.component";
import {ChartjsComponent} from "./components/chartjs.component";
import {LeafletComponent} from "./components/leaflet.component";
import {HelloComponent} from "./components/hello.component";

@NgModule({
  declarations: [
    CardComponent,
    GroupComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    ChartjsComponent,
    LeafletComponent,
    HelloComponent,
    PadDirective,
    MarginDirective,
    BorderDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkDirective,
    IfLoggedDirective,
    IfSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    FormatPipe,
    TimesAgoPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    GroupComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    ChartjsComponent,
    LeafletComponent,
    HelloComponent,
    PadDirective,
    MarginDirective,
    BorderDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    IfSigninDirective,
    LoaderDirective,
    FormatPipe,
    TimesAgoPipe
  ]
})
export class SharedModule { }
