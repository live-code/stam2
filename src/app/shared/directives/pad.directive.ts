import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[stPad]'
})
export class PadDirective {
  @Input('stPad') value: number = 0;

  @HostBinding() get class() {
    return 'p-' + this.value
  }

}
