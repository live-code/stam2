import {Directive, ElementRef, Input, Renderer2, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "../../core/auth/auth.service";

@Directive({
  selector: '[stIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() stIfRoleIs: 'admin' | 'moderator' | undefined;

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
    this.authService.auth$
      .subscribe(auth => {
        if(!!auth && auth.role === this.stIfRoleIs) {
          this.view.createEmbeddedView(template)
        } else {
          this.view.clear();
        }
      })
  }
}
