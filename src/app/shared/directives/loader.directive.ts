import {ComponentRef, Directive, Input, Type, ViewContainerRef} from '@angular/core';
import {CardComponent} from "../components/card.component";
import {HelloComponent} from "../components/hello.component";
import {LeafletComponent} from "../components/leaflet.component";

export const COMPONENTS: { [key: string]: Type<any> } = {
  card: CardComponent,
  hello: HelloComponent,
  leaflet: LeafletComponent
}

export interface Config {
  type: string;
  data?: any;
}

@Directive({
  selector: '[stLoader]'
})
export class LoaderDirective {
  @Input('stLoader') cfg!: Config;

  constructor(
    private view: ViewContainerRef
  ) {
  }

  ngOnInit() {
    try {
      const compo: ComponentRef<any> = this.view.createComponent(COMPONENTS[this.cfg.type])
      for (let key in this.cfg?.data) {
        compo.instance[key] = this.cfg.data[key];
      }
    } catch(e) {
      // throw new Error('this component is not supported')
    }

  }

}
