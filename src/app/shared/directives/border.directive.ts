import {Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[stBorder]'
})
export class BorderDirective {
  @Input() stBorder: 'dashed' | 'solid' | 'none' = 'solid';
  @Input() color: string = 'black';

  ngOnChanges() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'border',
      `2px ${this.stBorder} ${this.color}`
    )
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }


}
