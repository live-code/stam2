import {Directive, ElementRef, HostBinding, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[stMargin]'
})
export class MarginDirective {
  @Input() set stMargin(val: number) {
    // this.el.nativeElement.style.margin = `${10 * val}px`
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      `${10 * val}px`
    )
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {

  }


}
