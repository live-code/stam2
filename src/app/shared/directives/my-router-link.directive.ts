import {Directive, ElementRef, Input, Renderer2} from '@angular/core';
import {NavigationEnd, Router, RouterLink} from "@angular/router";
import {PadDirective} from "./pad.directive";

@Directive({
  selector: '[stMyRouterLink]'
})
export class MyRouterLinkDirective {
  @Input() stMyRouterLink!: string;

  constructor(
    private router: Router,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {
    this.router.events
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          const requiredPath = this.el.nativeElement.getAttribute('routerLink')
          if  (requiredPath && ev.url.includes(requiredPath)) {
            this.renderer.addClass(this.el.nativeElement, this.stMyRouterLink)
          } else {
            this.renderer.removeClass(this.el.nativeElement, this.stMyRouterLink)
          }
        }
      })
  }

  ngOnInit() {
    console.log(this.el.nativeElement.getAttribute('routerLink'))


  }
}
