import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[stUrl]'
})
export class UrlDirective {
  @Input() stUrl: string | undefined;

  constructor(private el: ElementRef<HTMLElement>) {}

  @HostListener('click')
  clickMe() {
    window.open(this.stUrl)
  }

}
