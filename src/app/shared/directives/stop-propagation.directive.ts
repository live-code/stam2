import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[stStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  clickMe(event: MouseEvent) {
    event.stopPropagation();
  }

}
