import {Directive, ElementRef, Renderer2, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "../../core/auth/auth.service";

@Directive({
  selector: '[stIfSignIn]'
})
export class IfSigninDirective {

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
    this.authService.isLogged$
      .subscribe(isLogged => {
        if(isLogged) {
          this.view.createEmbeddedView(template)
        } else {
          this.view.clear();
        }
      })
  }
}
