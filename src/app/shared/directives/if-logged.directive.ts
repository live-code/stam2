import {Directive, ElementRef, Renderer2} from '@angular/core';
import {AuthService} from "../../core/auth/auth.service";

@Directive({
  selector: '[stIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
    this.authService.isLogged$
      .subscribe(isLogged => {
        this.renderer.setStyle(
          this.el.nativeElement,
          'display',
          isLogged ? null : 'none'
        )
      })

  }

}
