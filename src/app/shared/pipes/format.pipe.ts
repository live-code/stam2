import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'format',
})
export class FormatPipe implements PipeTransform {

  constructor() {
  }
  transform(
    value: number,
    formatType: 'mb' | 'byte' = 'mb'
  ): string {
    console.log('qui!', value)
    switch (formatType) {
      case "mb":
        return `${value * 1000}mb`
      case "byte":
        return `${value * 1000000}bytes`
    }
  }

}
