import { Pipe, PipeTransform } from '@angular/core';
import {formatDistance} from "date-fns"

@Pipe({
  name: 'timesAgo'
})
export class TimesAgoPipe implements PipeTransform {

  transform(date: number | Date, ...args: unknown[]): unknown {
    return formatDistance(date, new Date(), { addSuffix: true })
  }

}
