import { AfterContentInit, Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { GroupComponent } from './group.component';

@Component({
  selector: 'st-accordion',
  template: `
    <div style="border: 1px solid black; padding: 20px">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements OnInit, AfterContentInit {
  @ContentChildren(GroupComponent) groups!: QueryList<GroupComponent>

  constructor() { }

  ngOnInit(): void {
    console.log(this.groups)
  }


  ngAfterContentInit() {
    this.groups.toArray()[0].opened = true;

    this.groups.toArray().forEach(g => {
      g.headerClick.subscribe(() => {
        this.closeAll()
        g.opened = !g.opened;
      })
    })
  }

  private closeAll() {
    this.groups.toArray().forEach(g => {
      g.opened = false;
    })
  }
}
