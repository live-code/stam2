import {CardComponent} from "./card.component";
import {GroupComponent} from "./group.component";
import {AccordionComponent} from "./accordion.component";
import {RowComponent} from "./row.component";
import {ColComponent} from "./col.component";
import {ChartjsComponent} from "./chartjs.component";
import {LeafletComponent} from "./leaflet.component";
import {HelloComponent} from "./hello.component";

export const COMPONENTS = [
  CardComponent,
  GroupComponent,
  AccordionComponent,
  RowComponent,
  ColComponent,
  ChartjsComponent,
  LeafletComponent,
  HelloComponent
]

