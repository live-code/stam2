import { Component, HostBinding, Input, OnInit, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'st-col',
  template: `
      <ng-content></ng-content>
  `,
  styles: [
  ],
  host: {

  }
})
export class ColComponent implements OnInit {
  @Input() size: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | undefined;

  @HostBinding() get class() {
    return 'col-' + this.parent.mq + '-' + this.size;
  }

  constructor(@Optional() private parent: RowComponent) {
    console.log(this.parent)
    if (!this.parent) {
      throw new Error('you cannot use Col without Row')
    }
  }

  ngOnInit(): void {
  }

}
