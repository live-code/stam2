import { AfterViewInit, Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import {
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  Decimation,
  Filler,
  Legend,
  LinearScale,
  Tooltip
} from 'chart.js';
Chart.register(BarElement, LinearScale, BarController, CategoryScale, Decimation, Filler, Legend, Tooltip);

@Component({
  selector: 'st-chartjs',
  template: `
    <canvas #host width="400" height="400"></canvas>
  `,
})
export class ChartjsComponent  {
  @Input() temps: number[] = []
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLCanvasElement>
  chart!: Chart;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['temps'].firstChange) {
      this.init();
    }
    this.chart.data.datasets[0].data = this.temps;
    this.chart.update()
  }

  init() {
    const ctx: CanvasRenderingContext2D | null = this.host.nativeElement.getContext('2d');

    this.chart = new Chart(ctx as any, {
      type: 'bar',
      data: {
        labels: ['L', 'M', 'M', 'G', 'V', 'S'],
        datasets: [{
          label: '# of Votes',
          data: [],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    })
  }


}
