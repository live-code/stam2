import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'st-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class RowComponent implements OnInit {
  @Input() mq: 'sm' | 'md' | 'lg' = 'sm';

  constructor() { }

  ngOnInit(): void {
  }

}
