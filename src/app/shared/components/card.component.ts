import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

interface User {
  name: string;
}

/**
 * Bellisima card
 */
@Component({
  selector: 'st-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card">
      <div class="card-header" [ngClass]="headerCls"
           (click)="toggle()">
        {{title}}
        <div class="pull-right" *ngIf="icon">
          <i [class]="'fa fa-' + icon" (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
    {{render()}}
  `,
})
export class CardComponent {
  /**
   * il titolo della card
   */
  @Input() title: any;
  @Input() icon: string | undefined;
  @Input() opened = true;
  @Output() openedChange = new EventEmitter<boolean>()
  @Input() headerCls: string = ''
  @Output() iconClick = new EventEmitter()

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit()
  }

  toggle() {
   // this.opened = !this.opened;
    this.openedChange.emit(this.opened)
  }

  render() {
    console.log('card')
  }
}
