import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'st-group',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card">
      <div class="card-header" (click)="headerClick.emit()">
        {{title}}
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class GroupComponent {
  @Input() title: string | undefined;
  @Input() opened = false;
  @Output() headerClick = new EventEmitter()
}
