import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'st-leaflet',
  template: `
    <div #map class="map"></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('map', { static: true}) mapReference!: ElementRef<HTMLDivElement>;
  @Input() coords: [number, number] | null = null;
  @Input() zoom: number = 5;
  map!: L.Map;

  ngOnInit() {
    if (!this.map) {
      this.init();
      this.updateCoords();
    }
  }


  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if(changes['coords'] && changes['coords'].firstChange) {
      this.init()
    }
    if (changes['coords']) {
      this.updateCoords()
    }
    if (changes['zoom']) {
      this.updateZoom()
    }
  }

  init() {
    if (this.coords) {
      this.map = L.map(this.mapReference.nativeElement)

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);

      L.marker(this.coords).addTo(this.map)
        .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
        .openPopup();
    }
  }

  private updateCoords() {
    if (this.coords) {
      this.map.setView(this.coords, this.zoom);
    }
  }
  private updateZoom() {
    this.map.setZoom(this.zoom);
  }
}
