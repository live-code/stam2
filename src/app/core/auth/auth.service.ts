import { Injectable } from '@angular/core';
import {BehaviorSubject, interval, map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

export interface Auth {
  token: string;
  role: string;
  displayName: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _auth$ = new BehaviorSubject<Auth | null>(null);
  public auth$ = this._auth$.asObservable();

  users$ = this.http.get<any[]>('https://jsonplaceholder.typicode.com/users')

  constructor(private http: HttpClient) {
  }

  login() {
    setTimeout(() => {
      const res = {
        token: 'wfhweohf',
        role: 'moderator',
        displayName: 'Fabio Biondi'
      }
      this._auth$.next(res);
    }, 500)
  }

  logout() {
    this._auth$.next(null);
  }

  get token$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      )
  }

  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      )
  }

}

