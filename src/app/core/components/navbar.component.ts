import {Component, Inject, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'st-navbar',
  template: `
   <button routerLink="login" stMyRouterLink="active">login</button>
   <button stIfLogged routerLink="uikit1" routerLinkActive="active">uikit1</button>
   <button *stIfRoleIs="'admin'" routerLink="uikit2" routerLinkActive="active">uikit2</button>

   <button *stIfSignIn (click)="authService.logout()" >logout</button>

   <input type="text" stIfLogged>
   <ng-template #message>
     <h1>ciao ciao </h1>
   </ng-template>
   <hr>
  `,
  styles: [`
    .active {background-color: red}
  `]
})
export class NavbarComponent implements OnInit {
  @ViewChild('message') msg!: TemplateRef<any>;

  constructor(
    private view: ViewContainerRef,
    @Inject('FIREBASE') fb: string, public authService: AuthService
  ) {
    console.log(fb)
  }

  ngOnInit(): void {

    setTimeout(() => {
      this.view.createEmbeddedView(this.msg)
    }, 5000)
  }

}
