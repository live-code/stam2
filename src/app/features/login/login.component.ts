import {
  AfterViewInit,
  Component,
  ComponentRef,
  OnInit,
  TemplateRef,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../../shared/components/card.component';
import { GroupComponent } from '../../shared/components/group.component';
import { HelloComponent } from '../../shared/components/hello.component';
import { LeafletComponent } from '../../shared/components/leaflet.component';
import {AuthService} from "../../core/auth/auth.service";
import {map} from "rxjs";
import {Config} from "../../shared/directives/loader.directive";

@Component({
  selector: 'st-login',
  template: `
    <h1>{{'Login'}}</h1>

    <h2>{{10 | format}}</h2>
    <h2>{{10 | format: 'mb'}}</h2>
    <h2>{{10 | format: 'byte'}}</h2>

    {{1646217006000 | timesAgo}}


    <input type="text" ngModel>
    <button (click)="authService.login()">login</button>
    <hr>
    <div
      *ngFor="let cfg of list"
      [stLoader]="cfg"
    ></div>
  `,
})
export class LoginComponent {
  list: Config[] = [
    {
      type: 'cards',
      data: {
        title: 'pippo',
        opened: true
      }
    },
    {
      type: 'leaflet',
      data: {
        coords: [42, 13]
      }
    },
    {
      type: 'hello',
    },
  ]
  constructor(public authService: AuthService) {
  }
}
