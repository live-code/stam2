import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'st-uikit2',
  template: `


    <div (click)="parent()" style="background-color: red; padding: 10px">
      <div stStopPropagation (click)="child()" style="background-color: purple; padding: 10px">
        content
      </div>
    </div>


    <button stUrl="http://www.google.com">visit url</button>


    <input type="text" ngModel>
    <div [stPad]="value">
      Padding works!
    </div>
    <div [stMargin]="value">
      Margin Example!
    </div>

    <div
      (click)="color = 'red'"
      stBorder="dashed" [color]="color">
      border
    </div>


    <button (click)="value = value + 1">+</button>
  `,
})
export class Uikit2Component implements OnInit {
  value: number = 1;
  color: string = 'cyan';

  constructor() { }

  ngOnInit(): void {
  }

  child() {
    console.log('child')
  }

  parent() {
    console.log('parent')
  }
}
