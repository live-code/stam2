import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogListItemComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    ReactiveFormsModule
  ]
})
export class CatalogModule { }
