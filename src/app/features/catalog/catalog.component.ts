import { Component } from '@angular/core';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'st-catalog',
  template: `
    <st-catalog-form
      (save)="catalogService.save($event)"
    ></st-catalog-form>
    
    <st-catalog-list
      [users]="catalogService.users"
      (deleteUser)="catalogService.deleteUser($event)"
    ></st-catalog-list>
  `,
})
export class CatalogComponent  {
  constructor(public catalogService: CatalogService) {
    catalogService.getAll()
  }

}
