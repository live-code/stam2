import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'st-catalog-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li (click)="opened = !opened">
      {{user.name}}
      <i class="fa fa-trash" (click)="deleteUser.emit(user)"></i>

      <div *ngIf="opened">
        body...
      </div>
    </li>
  `,
})
export class CatalogListItemComponent {
  @Input() user!: User;
  @Output() deleteUser = new EventEmitter<User>()
  opened = false;
}
