import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CatalogService } from '../services/catalog.service';
import { User } from '../../../model/user';

@Component({
  selector: 'st-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <st-catalog-list-item 
      *ngFor="let user of users" 
      [user]="user"
      (deleteUser)="deleteUser.emit($event)"
    >
    </st-catalog-list-item>
  `,
  styles: [
  ]
})
export class CatalogListComponent {
  @Input() users: User[ ] = []
  @Output() deleteUser = new EventEmitter<User>()

  // ...

}
