import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CatalogService } from '../services/catalog.service';
import { User } from '../../../model/user';

@Component({
  selector: 'st-catalog-form',
  template: `
    <form [formGroup]="form"
          (submit)="save.emit(form.value)">
      <input type="text" formControlName="name">
      <input type="text" formControlName="email">
      <button type="button" (click)="reset()">RESET</button>
      <button type="submit">SAVE</button>
    </form>
  `,
})
export class CatalogFormComponent {
  @Output() save = new EventEmitter<User>()
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      name: '',
      email: '',
      phone: '',
    })
  }

  reset() {
    this.form.reset()
  }
}
