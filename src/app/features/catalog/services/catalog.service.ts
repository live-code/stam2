import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';

type UserForm = Pick<User, 'name' | 'email'>;

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  users: User[] = []

  constructor(private http: HttpClient) {}

  getAll() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  deleteUser(user: User) {
    const { id } = user;
    this.http.delete(`http://localhost:3000/users/${id}`)
      .subscribe(() => {
        //const index = this.users.findIndex(u => u.id === id)
        //this.users.splice(index, 1)
        this.users = this.users.filter(u => u.id !== user.id)
      })
  }


  save(user: UserForm) {
    this.http.post<User>('http://localhost:3000/users', user)
      .subscribe(res => {
        // this.users.push(res);
        this.users = [...this.users, res]
      })
  }
}
