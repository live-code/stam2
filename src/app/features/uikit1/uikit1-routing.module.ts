import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit1Component } from './uikit1.component';

const routes: Routes = [{ path: '', component: Uikit1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit1RoutingModule { }
