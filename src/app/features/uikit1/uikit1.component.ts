import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { GroupComponent } from '../../shared/components/group.component';

@Component({
  selector: 'st-uikit1',
  template: `
    <st-leaflet [coords]="coords" [zoom]="zoom"></st-leaflet>
    <button (click)="coords = [43, 13]">1</button>
    <button (click)="coords = [41, 11]">2</button>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <hr>
    
    <st-chartjs [temps]="temps"></st-chartjs>
    <div class="row">
      <div class="col-md-8">1</div>
      <div class="col-md-4">2</div>
    </div>
    
    <st-row mq="sm">
      <st-col [size]="8">1</st-col>
      <st-col [size]="4">
        lorem
      </st-col>
    </st-row>
  
    <st-card
      [title]="'pippo'" 
      [(opened)]="visible"
      (openedChange)="doSomething($event)"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aperiam architecto assumenda facilis id ipsam iste, maiores officia officiis quo, quod sit sunt tempora temporibus ut voluptas! Quasi, quidem?
    </st-card>
    
    <br>
    <st-accordion>
      <st-group title="one">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aperiam architecto assumenda facilis id ipsam iste, maiores officia officiis quo, quod sit sunt tempora temporibus ut voluptas! Quasi, quidem?
      </st-group>
      <st-group title="two">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aperiam architecto assumenda facilis id ipsam iste, maiores officia officiis quo, quod sit sunt tempora temporibus ut voluptas! Quasi, quidem?
      </st-group>
      <st-group title="three">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci aperiam architecto assumenda facilis id ipsam iste, maiores officia officiis quo, quod sit sunt tempora temporibus ut voluptas! Quasi, quidem?
      </st-group>
    </st-accordion>
    
  
  `,
  styles: [
  ]
})
export class Uikit1Component  {
  visible = false;
  temps: number[] = [];
  coords: [number, number] = [44, 12];
  zoom: number = 10;

  constructor() {
    setTimeout(() => {
      this.temps = [16, 13, 111, 22, 3, 4];
    }, 500)
    setTimeout(() => {
      this.temps = [26, 33, 33, 22, 3, 4];
    }, 3000)
  }

  doSomething(value: boolean) {
    console.log(value)
  }

}
