import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { PadDirective } from './shared/directives/pad.directive';
import {SharedModule} from "./shared/shared.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: 'FIREBASE',
      useFactory: () => {
        return {
          url: 'http://www.google.com'
        }
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
