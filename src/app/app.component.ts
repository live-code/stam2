import { Component } from '@angular/core';

@Component({
  selector: 'st-root',
  template: `
    <st-navbar></st-navbar>
    <div class="container mt-2">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'stam2';
}
